const express = require('express');
const router = new express.Router();
const {asyncWrapper} = require('./helpers/helpers');
const {authMiddleware} = require('../routers/middlewares/authMiddleware');
const {
  dbGetUser,
  dbRmUser,
  dbChangeUserPass,
} = require('../controllers/usersController');

router.get('/', asyncWrapper(authMiddleware), asyncWrapper(dbGetUser));

router.delete('/', asyncWrapper(authMiddleware), asyncWrapper(dbRmUser));

router.patch('/', asyncWrapper(authMiddleware), asyncWrapper(dbChangeUserPass));

module.exports = router;
