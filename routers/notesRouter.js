const express = require('express');
const router = new express.Router();
const {asyncWrapper} = require('./helpers/helpers');
const {authMiddleware} = require('../routers/middlewares/authMiddleware');
const {
  dbAddNote,
  dbGetMyNotes,
  dbGetNoteById,
  dbUpdateNote,
  dbCompleteNote,
  dbRmNote,
} = require('../controllers/notesController');

router.get('/', asyncWrapper(authMiddleware), asyncWrapper(dbGetMyNotes));

router.post('/', asyncWrapper(authMiddleware), asyncWrapper(dbAddNote));

router.get('/:id', asyncWrapper(authMiddleware), asyncWrapper(dbGetNoteById));

router.put('/:id', asyncWrapper(authMiddleware), asyncWrapper(dbUpdateNote));

router.patch(
    '/:id',
    asyncWrapper(authMiddleware),
    asyncWrapper(dbCompleteNote),
);

router.delete('/:id', asyncWrapper(authMiddleware), asyncWrapper(dbRmNote));

module.exports = router;
