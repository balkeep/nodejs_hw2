const {checkToken} = require('../helpers/checks');
const {getDataFromToken} = require('../helpers/authToken');

module.exports.authMiddleware = async (req, res, next) => {
  const [, token] = req.headers['authorization'].split(' ');

  res.locals = getDataFromToken(checkToken(token));

  next();
};
