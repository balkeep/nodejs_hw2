const bcrypt = require('bcrypt');
const {
  checkUser,
  checkPassword,
  isYourNote,
} = require('../../routers/helpers/checks');
const {validUser} = require('../../routers/helpers/validation');
const {WrongNoteIdError} = require('../../Errors');
const {User} = require('../../models/UserModel');
const {Note} = require('../../models/NoteModel');

const getNoteFromDb = async (id) => {
  try {
    const note = await Note.findById(id);
    if (!note) throw new WrongNoteIdError();
    return note;
  } catch (e) {
    throw new WrongNoteIdError();
  }
};

module.exports.createUser = async ({username, password}) => {
  const user = new User({
    username,
    password: await bcrypt.hash(password, 10),
  });

  await user.save();
};

module.exports.getUserById = async (id) => checkUser(await User.findById(id));

module.exports.getUserByName = async (username) =>
  checkUser(await User.findOne({username}));

module.exports.rmUser = async (id) => {
  const user = checkUser(await User.findById(id));
  await user.remove();
};

module.exports.updatePasswd = async (id, oldPassword, newPassword) => {
  const user = checkUser(await User.findById(id));
  await checkPassword(oldPassword, user.password);
  await validUser({username: user.username, password: newPassword});
  user.password = await bcrypt.hash(newPassword, 10);
  await user.save();
};
module.exports.createNote = async (userId, text) => {
  const note = new Note({userId, text});
  await note.save();
};

module.exports.getNotesOfUser = async ({_id: userId, offset, limit}) =>
  await Note.find({userId}, null, {skip: +offset, limit: +limit});

module.exports.getNoteById = async (id, userId) => {
  const note = await getNoteFromDb(id);
  isYourNote(userId, note.userId);

  return note;
};

module.exports.updateNoteById = async (id, text, userId) => {
  const note = await getNoteFromDb(id);
  isYourNote(userId, note.userId);
  note.text = text;

  await note.save();
};

module.exports.reverseCompletionOfNoteById = async (id, userId) => {
  const note = await getNoteFromDb(id);
  isYourNote(userId, note.userId);
  note.completed = !note.completed;
  await note.save();
};

module.exports.rmNoteById = async (id, userId) => {
  const note = await getNoteFromDb(id);
  isYourNote(userId, note.userId);
  note.remove();
};
