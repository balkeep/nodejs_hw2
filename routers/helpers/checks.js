const {
  NoUserError,
  NoTokenError,
  BadPasswordError,
  NotYourNoteError,
} = require('../../Errors');
const bcrypt = require('bcrypt');

module.exports.checkUser = (user) => {
  if (user) return user;
  throw new NoUserError();
};

module.exports.checkToken = (token) => {
  if (token) return token;
  throw new NoTokenError();
};

module.exports.checkPassword = async (passOne, passTwo) => {
  if (await bcrypt.compare(passOne, passTwo)) return true;
  throw new BadPasswordError();
};

module.exports.isYourNote = (yourId, noteUserId) => {
  if (yourId.toString() === noteUserId.toString()) return true;
  throw new NotYourNoteError();
};
