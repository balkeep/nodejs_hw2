const Joi = require('joi');

module.exports.validUser = (user) =>
  Joi.object()
      .keys({
        username: Joi.string().alphanum().min(3).max(30).required(),
        password: Joi.string().pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')),
      })
      .validateAsync(user);

module.exports.validNote = (note) =>
  Joi.object()
      .keys({
        text: Joi.string().required(),
      })
      .validateAsync(note);

module.exports.validOffsetLimit = (note) =>
  Joi.object()
      .keys({
        offset: Joi.number().min(0),
        limit: Joi.number().min(0),
      })
      .validateAsync(note);
