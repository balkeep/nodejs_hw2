const express = require('express');
const router = new express.Router();

const {asyncWrapper} = require('./helpers/helpers');
const {login, register} = require('../controllers/authController');

router.post('/register', asyncWrapper(register));

router.post('/login', asyncWrapper(login));

module.exports = router;
