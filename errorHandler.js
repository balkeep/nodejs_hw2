module.exports.errorHandler = (e, req, res, next) => {
  if (e.statusCode) {
    res.status(e.statusCode).json({message: e.message});
  } else {
    res.status(500).json({message: 'Internal Server Error'});
  }
};
