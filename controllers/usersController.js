const {
  getUserById,
  rmUser,
  updatePasswd,
} = require('../routers/helpers/dbHelpers');

module.exports.dbGetUser = async (req, res) => {
  const {_id} = res.locals;

  const {username, createdDate} = await getUserById(_id);

  res.json({user: {_id, username, createdDate}});
};

module.exports.dbRmUser = async (req, res) => {
  const {_id} = res.locals;

  await rmUser(_id);

  res.json({message: 'User removed successfully!'});
};

module.exports.dbChangeUserPass = async (req, res) => {
  const {_id, username} = res.locals;
  const {oldPassword, newPassword} = req.body;

  await updatePasswd(_id, oldPassword, newPassword);

  res.json({message: `Password for ${username} updated successfully!`});
};
