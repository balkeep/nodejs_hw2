const {
  validNote,
  validOffsetLimit,
} = require('../routers/helpers/validation');
const {
  createNote,
  getNotesOfUser,
  getNoteById,
  updateNoteById,
  reverseCompletionOfNoteById,
  rmNoteById,
} = require('../routers/helpers/dbHelpers');

module.exports.dbAddNote = async (req, res) => {
  const note = req.body;
  await validNote(note);

  const {_id} = res.locals;
  await createNote(_id, note.text);

  res.json({message: 'Note created successfully!'});
};

module.exports.dbGetMyNotes = async (req, res) => {
  const params = req.query;
  await validOffsetLimit(params);

  const {_id} = res.locals;
  const notes = await getNotesOfUser({_id, ...params});

  res.json({notes});
};

module.exports.dbGetNoteById = async (req, res) => {
  const {id} = req.params;
  const {_id} = res.locals;

  const note = await getNoteById(id, _id);

  res.json({note});
};

module.exports.dbUpdateNote = async (req, res) => {
  const {id} = req.params;
  const note = req.body;

  await validNote(note);
  const {_id} = res.locals;
  await updateNoteById(id, note.text, _id);

  res.json({message: 'Note updated successfully!'});
};

module.exports.dbCompleteNote = async (req, res) => {
  const {id} = req.params;
  const {_id} = res.locals;

  await reverseCompletionOfNoteById(id, _id);

  res.json({message: 'Note updated successfully!'});
};

module.exports.dbRmNote = async (req, res) => {
  const {id} = req.params;
  const {_id} = res.locals;

  await rmNoteById(id, _id);

  res.json({message: 'Note removed successfully!'});
};
