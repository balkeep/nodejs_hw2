const {validUser} = require('../routers/helpers/validation');
const {createUser, getUserByName} = require('../routers/helpers/dbHelpers');
const {checkPassword} = require('../routers/helpers/checks');
const {getToken} = require('../routers/helpers/authToken');

module.exports.register = async (req, res) => {
  const user = req.body;

  await validUser(user);
  await createUser(user);

  res.json({message: 'User created successfully!'});
};

module.exports.login = async (req, res) => {
  const {username, password} = req.body;

  const user = await getUserByName(username);
  await checkPassword(password, user.password);

  res.json({message: 'Success', jwt_token: getToken(user._id, user.username)});
};
