require('dotenv').config();
const morgan = require('morgan');
const express = require('express');
const app = express();

const {PORT} = require('./config');
const {startDB} = require('./dbConnector');

const {errorHandler} = require('./errorHandler');
const usersRouter = require('./routers/usersRouter');
const notesRouter = require('./routers/notesRouter');
const authRouter = require('./routers/authRouter');

app.use(morgan('dev'));
app.use(express.json());

app.use('/api/users/me', usersRouter);
app.use('/api/notes', notesRouter);
app.use('/api/auth', authRouter);
app.use(errorHandler);

startDB();
app.listen(PORT, () => {
  console.log(`Server started at port ${PORT}!`);
});
