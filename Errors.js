module.exports.NoUserError = class NoUserError extends Error {
/**
 * An Error to be thrown when request contains wrong user data
 * @param {String} message Message containing Error specifics
*/
  constructor(message = 'User does not exist!') {
    super(message);
    this.statusCode = 400;
  }
};

module.exports.BadPasswordError = class BadPasswordError extends Error {
/**
 * An Error to be thrown when wrong or wrongly formatted password is provided
 * @param {String} message Message containing Error specifics
*/
  constructor(message = 'Wrong password!') {
    super(message);
    this.statusCode = 400;
  }
};

module.exports.NoTokenError = class NoTokenError extends Error {
/**
 * An Error to be thrown when the token is not provided with the request
 * @param {String} message Message containing Error specifics
*/
  constructor(message = 'No JWT token found!') {
    super(message);
    this.statusCode = 401;
  }
};

module.exports.InvalidTokenError = class InvalidTokenError extends Error {
  /**
   * An Error to be thrown when the token is invalid
   * @param {String} message Message containing Error specifics
  */
  constructor(message = 'Invalid JWT token!') {
    super(message);
    this.statusCode = 401;
  }
};

module.exports.NotYourNoteError = class NotYourNoteError extends Error {
/**
 * An Error to be thrown when user tries to access notes that does not belong
 * to him
 * @param {String} message Message containing Error specifics
*/
  constructor(message = 'Not your Note!') {
    super(message);
    this.statusCode = 401;
  }
};

module.exports.WrongNoteIdError = class WrongNoteIdError extends Error {
/**
 * An Error to be thrown when incorrect note id is provided
 * @param {String} message Message containing Error specifics
*/
  constructor(message = 'Wrong Note ID!') {
    super(message);
    this.statusCode = 400;
  }
};
